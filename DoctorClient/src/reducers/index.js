import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import FoodReducer from './FoodReducer';
import AccountReducer from './AccountReducer';

export default combineReducers({
  auth: AuthReducer,
  food: FoodReducer,
  account: AccountReducer,
});