import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

import {BackHandler} from 'react-native'

export default class App extends Component {  

  
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  handleBackButton() {
    return true;
  }

  render() {
    

    const store= createStore(reducers, 
      {}, // inital state
      applyMiddleware(ReduxThunk)
      
    );
    return(
      
      <Provider store={store}>        
          <Router />
      </Provider>
    );
  }
}
