import React, { PureComponent } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';


import { Button, Icon, Text, CheckBox } from 'react-native-elements'

import {  EMERALD, GRAY, YELLOW, RED } from '../../services/CSSColor';

import { LOCAL_URL, SERVER_URL, RUNNING_URL } from '../../services/network';



class PlannedFoodItem extends PureComponent {
    
    
    

    render() {
        // console.log(this.props);    
        choices = this.props.plannedMenu[this.props.choice];
        
        amount = choices[this.props.data.index];
        return(
            <View style={styles.container} key={this.props.key}>                
                <View style={styles.card}>
                    <Image
                      style={styles.image}
                      source={{uri: RUNNING_URL + this.props.data.image}}
                      >
                    </Image>
                    <View style={styles.text}>
                        <Text>
                            {this.props.data.name}
                        </Text>
                        <Text>
                            Khẩu phần:  {this.props.data.portion} 
                        </Text>                        
                        <Text>
                            Số lượng: {amount}
                        </Text>
                    </View>                    

                        
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {                
        alignItems: "center",
        backgroundColor: "white",
    },
    card: {
        flexDirection: 'row',
        height: 100,
        backgroundColor: "white",
        justifyContent: 'center',        
        alignItems: 'center',        
        borderWidth: 1,
        borderTopWidth: 0,
        borderColor: EMERALD,
        width: "80%",     
    },
    image: {
        width: 100, 
        height: "100%",        
    },
    text: {
        fontSize: 16,
        color: "white",
        flex: 4,
    },
    button: {
        marginRight: 20,
        backgroundColor: 'white',
        borderColor: '#15aaf7',
        width: 35,
        height: 35,
        borderRadius: 100,
        borderWidth: 1,
    }    
});

const mapStateToProps = ( { food, auth, account } ) => {        
    const { plannedMenu } = food;
    return { plannedMenu };
};

export default connect(mapStateToProps,{  })(PlannedFoodItem);
