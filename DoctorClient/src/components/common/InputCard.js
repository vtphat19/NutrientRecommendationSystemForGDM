import React, { Component } from "react";
import { View, TextInput } from "react-native";
import { connect } from 'react-redux';
import { updateInput } from '../../actions/AccountActions';

import {
  Text
} from "react-native-elements";


class InputCard extends Component {

  render(){
      return(
        <View style={{marginTop: 10}}>
        <Text style={{fontSize: 18, textAlign: "left"}}>
          {this.props.label}
        </Text>
        <TextInput
          style={{ borderColor: 'gray', borderBottomWidth: 1, fontSize: 16}}
          onChangeText={(text) => this.props.updateInput(text, this.props.state_name, this.props.user_name)}
          value={this.props.text}
          editable={this.props.editable}
        />
        
      </View>          
      );
  }
}

export default connect(null, { updateInput })(InputCard);
