import React, { PureComponent } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";

import {
  Button,
  ThemeProvider,
} from "react-native-elements";

import { Theme } from "../services/CSSColor";

import InputCard from '../components/common/InputCard';


import { logout } from '../actions/AuthActions';

import { getUserInfo, getPatientDetail, updateProfile } from '../actions/AccountActions';
import NavBar from './common/NavBar';



class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.props.getUserInfo(this.props.token); // this action will return a role
    console.log('call constructor');
    console.log(this.props.role);

    // this call is required, when user logged in, he turns off and open again, the contructor will be call!
    if (this.props.role == "patient") {
      console.log('call inside if'); 
      console.log('call me too');     
      this.props.getPatientDetail(this.props.token, this.props.user_id);
      
    } 
    // for doctor, we just display his/her name  
  }


  updateInfo() {
    const payload = {
      "username": this.props.username,
      "first_name": this.props.first_name,
      "last_name": this.props.last_name,      
    }
    this.props.updateProfile(this.props.token, payload);
  }  

  onLogoutPress() {
    this.props.logout();
  }

  displayPatientInfo() {
    if (this.props.role == 'patient') {
      return(
        <View>
          <InputCard text={String(this.props.height)} label={"Height (cm)"}  editable={false}></InputCard>
          <InputCard text={String(this.props.weight)} label={"weight (kg)"}  editable={false}></InputCard>
          <InputCard text={String(this.props.activity)} label={"Activity"}  editable={false}></InputCard>
          <InputCard text={String(this.props.stress_factor)} label={"Stress factor"} editable={false}></InputCard>
        </View>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ThemeProvider theme={Theme}>
          <NavBar title="Profile"></NavBar>
          <View style={styles.body}>
            <ScrollView style={{width:"80%"}} 
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false} >
              <View style={{marginTop: 10}}>
              
                <InputCard text={this.props.first_name} label={"First name"} state_name={"first_name"}></InputCard>
                <InputCard text={this.props.last_name} label={"Last name"} state_name={"last_name"}></InputCard>
                <InputCard text={this.props.email} label={"Email"} editable={false}></InputCard>
                
              </View>
                            
              {this.displayPatientInfo()}

              <View>
              <Button 
                  title="Save Changes" 
                  buttonStyle={{borderColor: 'black', borderWidth: 1, marginTop: 10}} 
                  onPress={this.updateInfo.bind(this)}
                />
              <Button 
                  title="Log Out" 
                  buttonStyle={{borderColor: 'black', borderWidth: 1, marginTop: 10}} 
                  onPress={this.props.logout.bind(this)}
                />                              
              </View>
            </ScrollView>
          </View>
        </ThemeProvider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  linearGradient: {
    width: "100%",
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center"
  },
  body: {
    width: "100%",
    flex: 6,
    alignItems: "center"
  },
  inputContainer: {
    flex: 1,
    flexDirection: "row"
  },
  label: {
    color: "black"
  },
  input: {
    flex: 3,
    borderColor: "red",
    borderWidth: 2
  }
});

const mapStateToProps = ( { auth, account } ) => {
  token = auth.token;  
  user_id = auth.user_id;
  const { username, first_name, last_name, email, height, weight, activity, stress_factor,  role } = account;        
  return { user_id, username, first_name, last_name, email, token, height, weight, activity, stress_factor,  role  };
}


export default connect(
  mapStateToProps,
  { logout, getUserInfo, getPatientDetail, updateProfile }
)(Profile);
