import React, { PureComponent } from 'react';
import { View, StyleSheet, ScrollView, Picker } from 'react-native';
import { connect } from 'react-redux';
import { foodFetch, amountCalculate } from '../actions/FoodActions';
import PlannedFoodItem from './common/PlannedFoodItem ';
import { Button, Text, Overlay    } from 'react-native-elements';
import {  EMERALD, CORAL, BG, GRAY } from '../services/CSSColor';
import NavBar from './common/NavBar';

const BREAKFAST = "morning";
const LUNCH = "lunch";
const DINNER = "dinner";


console.disableYellowBox = true;


class PlannedMenu extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            navTitle: 'Planned Menu',            
            optionToDisplay: 'choice 1',
            
        }
        
    }
 

    _renderItem = ({item}) => (        
        <PlannedFoodItem
            data={item}
            
        />
      );

    
    renderList(time, type) {

        switch (this.state.optionToDisplay) {
            case "choice 1":
                return this.props.result[time][type].map((item, key) => {
                    
                    return (
                        <PlannedFoodItem data={item} key={key} choice={'choice 1'} ></PlannedFoodItem>
                    );
                });

            case "choice 2":
                return this.props.result[time][type].map((item, key) => {                
                    return (
                        <PlannedFoodItem data={item} key={key}  choice={'choice 2'}></PlannedFoodItem>
                    );
                });                
            case "choice 3":
                    return this.props.result[time][type].map((item, key) => {                
                        return (
                            <PlannedFoodItem data={item} key={key}  choice={'choice 3'}></PlannedFoodItem>
                        );
                    });
            default:
                return this.props.result[time][type].map((item, key) => {                
                    return (
                        <PlannedFoodItem data={item} key={key}  choice={'choice 1'}></PlannedFoodItem>
                    );
                });                
        }

 
    }
    
    renderSection(time, type, title) {
        console.log('render section ', time, type);
        if ((this.props.mainMeal == BREAKFAST)) {
            return(
                <View>
                    <View style={{width: "100%", borderWidth: 1, borderColor: EMERALD, alignSelf: "center", borderTopWidth: 1}}>                        
                        <Text style={{color: CORAL, fontFamily: "Roboto", textAlign: "center", paddingTop: 0, marginTop: 0, fontSize: 18, fontWeight: "bold"}}>{title}</Text>
                    </View>
                    {this.renderList(time, type)}
                </View>
            );       
        } else if ((this.props.mainMeal == LUNCH)) {
            return(
                <View>
                <View style={{width: "100%", borderWidth: 1, borderColor: EMERALD, alignSelf: "center", borderTopWidth: 1}}>                        
                    <Text style={{color: CORAL, fontFamily: "Roboto", textAlign: "center", paddingTop: 0, marginTop: 0, fontSize: 18, fontWeight: "bold"}}>{title}</Text>
                </View>
                {this.renderList(time, type)}
            </View>                
            );
        } else { // dinner
            return(
                <View>
                <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, alignSelf: "center", borderTopWidth: 1}}>                        
                    <Text style={{color: CORAL, fontFamily: "Roboto", textAlign: "center", paddingTop: 0, marginTop: 0, fontSize: 18, fontWeight: "bold"}}>{title}</Text>
                </View>
                {this.renderList(time, type)}
            </View>                
            );
        }

    }

    renderMenu() {
        console.log('render menu ', this.props.mainMeal);
        if (this.props.mainMeal == BREAKFAST) {
            return (
            <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, alignSelf:"center"}}>                        
                <Text h4 style={{color: CORAL, fontFamily: "Roboto", textAlign: "center"}}>Morning</Text>
                {this.renderSection('morning', 'main', 'Main')}
                {this.renderSection('morning', 'dessert', 'Dessert')}
            </View>
            );
        } else if (this.props.mainMeal == LUNCH) {
            return(
                <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, alignSelf:"center", borderTopWidth: 0}}>
                    <Text h4 style={{color: CORAL, fontFamily: "Roboto", textAlign: "center"}}>Lunch</Text>
                    {this.renderSection('lunch', 'main', 'Main')}
                        {this.renderSection('lunch', 'dessert', 'Dessert')}
                </View>            
            );
        } else {
            return(
                <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, alignSelf:"center", borderTopWidth: 0}}>                        
                <Text h4 style={{color: CORAL, fontFamily: "Roboto", textAlign: "center"}}>Dinner</Text>
                {this.renderSection('dinner', 'main', 'Main')}
                {this.renderSection('dinner', 'dessert', 'Dessert')}                    
            </View>                 
            ) ;                
        }
    }    

    render() {
        if (typeof this.props.plannedMenu['choice 1'] != 'undefined') {
            return(
                <View style={styles.container}>        
                    <View style={{width: "100%"}}>
                        <NavBar title={this.state.navTitle} ></NavBar> 
                        <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, alignSelf:"center", alignItems: "center"}}>                                                                    
                                                                            
                            <Picker
                                mode="dialog"
                                selectedValue={this.state.optionToDisplay}
                                style={{width: "100%", textAlign: "center"}}
                                itemStyle={{textAlign: "center", color: "red"}}
                                onValueChange={(itemValue, itemIndex) =>
                                    {this.setState({optionToDisplay: itemValue});
                                    }
                            }>
                            <Picker.Item label="Choice 1" value="choice 1" />
                            <Picker.Item label="Choice 2" value="choice 2" />
                            <Picker.Item label="Choice 3" value="choice 3" />
    
                            
                        </Picker>                                     
                            
    
                        </View>                                
                    </View>
    
                     {/* Morning */}
                     <ScrollView style={{width: "100%"}}>                     
    
                        {/* <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, alignSelf:"center"}}>                        
                            <Text h4 style={{color: CORAL, fontFamily: "Roboto", textAlign: "center"}}>Morning</Text>
                        </View> */}
                        {this.renderMenu()}
                        {/* {this.renderSection('morning', 'main', 'Main')} */}
                        {/* {this.renderSection('morning', 'dessert', 'Dessert')} */}
                        {/* Lunch */}
                        {/* <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, alignSelf:"center", borderTopWidth: 0}}>
                            <Text h4 style={{color: CORAL, fontFamily: "Roboto", textAlign: "center"}}>Lunch</Text>
                        </View>                                                                                 */}
                        {/* {this.renderSection('lunch', 'main', 'Main')}
                        {this.renderSection('lunch', 'dessert', 'Dessert')} */}
                        {/* Dinner */}
                        {/* <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, alignSelf:"center", borderTopWidth: 0}}>                        
                            <Text h4 style={{color: CORAL, fontFamily: "Roboto", textAlign: "center"}}>Dinner</Text>
                        </View>                            */}
                        {/* {this.renderSection('dinner', 'main', 'Main')}
                        {this.renderSection('dinner', 'dessert', 'Dessert')}                     */}
           
                    </ScrollView>
                </View>
            );
        } else {
            return(
                <View style={styles.container}>        
                    <View style={{width: "100%"}}>
                        <NavBar title={this.state.navTitle} ></NavBar>                         
                    </View>
                    <View style={{flex: 1, alignSelf: "center", justifyContent: "center", width: "80%"}}>
                            <Text style={{textAlign: "center"}}>Nothing has been selected yet.</Text>
                            <Text style={{textAlign: "center"}}>Please go back to select your favourite food</Text>
                    </View>                    
                </View>                
            );
        }

    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',        
        alignItems: "center",
        backgroundColor: "white"
    },
    button: {
        backgroundColor: EMERALD, 
        width: "100%"
    },
    androidPicker: {
        color: '#6D6D6D',
        backgroundColor: '#FFF',
        marginBottom: 20,
        height: 40,
        alignSelf: 'center', 
        justifyContent:'center',
    }

});

function getResult(foodList) {

    copy_foodList = Object.create(foodList);
    const { morning, lunch, dinner } = copy_foodList;
    
    var result = new Object({
        "morning": {
            "main": new Array(),
            "dessert": new Array(),
        },
        "lunch": {
            "main": new Array(),
            "dessert": new Array(),
        },
        "dinner": {
            "main": new Array(),
            "dessert": new Array(),
        }
    });
    
    var i = 0;
    Array.from(morning.main).forEach(item => {        
        if (item.selected) {            
            item['index'] = i;
            result['morning']['main'].push(item);
            i = i +1;
        }     
    });
    Array.from(morning.dessert).forEach(item => {        
        if (item.selected) {            
            item['index'] = i;
            result['morning']['dessert'].push(item);
            i = i +1;
        }     
    });
    // Lunch
    Array.from(lunch.main).forEach(item => {        
        if (item.selected) {            
            item['index'] = i;
            result['lunch']['main'].push(item);
            i = i +1;
        }     
    });
    Array.from(lunch.dessert).forEach(item => {        
        if (item.selected) {            
            item['index'] = i;
            result['lunch']['dessert'].push(item);
            i = i +1;
        }     
    });
    // Dinner
    Array.from(dinner.main).forEach(item => {        
        if (item.selected) {            
            item['index'] = i;
            result['dinner']['main'].push(item);
            i = i +1;
        }     
    });
    Array.from(dinner.dessert).forEach(item => {        
        if (item.selected) {            
            item['index'] = i;
            result['dinner']['dessert'].push(item);
            i = i +1;
        }     
    });
    return result;
}

const mapStateToProps = ( { food, auth, account } ) => {        
    token = auth.token;
    user_id = account.user_id;
    foodList = food.foodList;
    selectedFood = food.selectedFood;
    mainMeal =  food.mainMeal
    const { plannedMenu } = food;

    
    result = getResult(foodList);

  
    return {  selectedFood, token, user_id, result, plannedMenu, mainMeal };
}

export default connect(mapStateToProps, { foodFetch, amountCalculate } )(PlannedMenu);
