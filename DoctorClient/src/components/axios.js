import * as axios from 'axios';

import { LOCAL_URL, SERVER_URL, RUNNING_URL } from '../services/network';

var instance = axios.create();


// instance.defaults.baseURL = 'http://10.0.2.2:8000/';
instance.defaults.baseURL = RUNNING_URL
// instance.defaults.baseURL = SERVER_URL
instance.defaults.timeout = 10000 //8000;
// instance.defaults.proxy =  false;

instance.defaults.xsrfCookieName = "csrftoken";
instance.defaults.xsrfHeaderName = "X-CSRFTOKEN"

instance.defaults.withCredentials = true

// Add a request interceptor
instance.interceptors.request.use(function (config) {
    // Do something before request is sent
    console.log('intercept .. request');
    console.log(config);
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
instance.interceptors.response.use(function (response) {
    // Do something with response data
    console.log('intercept .. response');
    console.log(response);    
    return response;
  }, function (error) {
    // Do something with response error
    return Promise.reject(error);
  });

export { instance as default };

