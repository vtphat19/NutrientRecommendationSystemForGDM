import React, { PureComponent } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { foodFetch, amountCalculate, resetSelection, amountCalculateMorning, amountCalculateAfternoon, amountCalculateEvening } from '../actions/FoodActions';
import FoodItem from './common/FoodItem';
import { Button, Text, Overlay    } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import {  EMERALD, CORAL, BG, GRAY } from '../services/CSSColor';
import NavBar from './common/NavBar';


// CONSTANT DECLARATIONS
const BREAKFAST = "morning";
const LUNCH = "lunch";
const DINNER = "dinner";
const LUNCH_BTN_LABEL = "Coninue planning your lunch";
const DINNER_BTN_LABEL = "Now planning your dinner";
const FINISH_BTN_LABEL = "Finish";

const MORNING_NAV_TITLE = "Morning Menu";
const LUNCH_NAV_TITLE = "Lunch Menu";
const DINNER_NAV_TITLE = "Dinner Menu";



console.disableYellowBox = true;


class FoodList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            mainMeal: BREAKFAST, // could be  breakfast, lunch, dinner
            displayMainMeal: true,
            btn_label: LUNCH_BTN_LABEL,
            navTitle: MORNING_NAV_TITLE,
            calcuting: false,
            isVisible: false,
        }
        
    }
    
    _renderItem = ({item}) => (        
        <FoodItem
            data={item}
            meal={{mainMeal: this.state.mainMeal, displayMainMeal: this.state.displayMainMeal }}
        />
      );

    componentWillMount() {
        console.log(this.props.user_id);
        this.props.foodFetch(this.props.token, this.props.user_id);
        
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
    }
    

    onButtonPress() {
   
        if (this.state.mainMeal == BREAKFAST) {

            selected = this.getSelectedFood(this.props.morning.main);
            if (selected.length > 0) {
                this.setState({
                    navTitle: LUNCH_NAV_TITLE,
                    mainMeal: LUNCH,
                    btn_label: DINNER_BTN_LABEL,
                    
                });
            } else {
                this.setState({ isVisible: true });
            }
            dataToSend= {
                patient_id: this.props.user_id,
                morning: {
                    main: this.getSelectedFood(this.props.morning.main),
                    dessert: this.getSelectedFood(this.props.morning.dessert),
                },
                lunch: {
                    main: {},
                    dessert: {},
                },
                dinner: {
                    main: {},
                    dessert: {},
                },
            }
            this.props.amountCalculateMorning(dataToSend, this.state.mainMeal);

        } else if (this.state.mainMeal == LUNCH) {
            console.log('LUNCH');
            selected = this.getSelectedFood(this.props.lunch.main);

            dataToSend= {
                patient_id: this.props.user_id,
                morning: {
                    main: {},
                    dessert: {},                        
                },
                lunch: {
                    main: this.getSelectedFood(this.props.lunch.main),
                    dessert: this.getSelectedFood(this.props.lunch.dessert),
                },
                dinner: {
                    main: {},
                    dessert: {},
                },
            }
            this.props.amountCalculateAfternoon(dataToSend, this.state.mainMeal);

            if (selected.length > 0 ) {
                this.setState({
                    navTitle: DINNER_NAV_TITLE,
                    mainMeal: DINNER,
                    btn_label: FINISH_BTN_LABEL,
                });
            } else {
                this.setState({ isVisible: true });                            
            }


        } else if (this.state.mainMeal == DINNER ) {
            selected = this.getSelectedFood(this.props.dinner.main);
            if (selected.length == 0) {
                this.setState({ isVisible: true });
            } else {
                // dataToSend= {
                //     patient_id: this.props.user_id,
                //     morning: {
                //         main: this.getSelectedFood(this.props.morning.main),
                //         dessert: this.getSelectedFood(this.props.morning.dessert),
                //     },
                //     lunch: {
                //         main: this.getSelectedFood(this.props.lunch.main),
                //         dessert: this.getSelectedFood(this.props.lunch.dessert),
                //     },
                //     dinner: {
                //         main: this.getSelectedFood(this.props.dinner.main),
                //         dessert: this.getSelectedFood(this.props.dinner.dessert),
                //     },                
                // };
                // Emit an action to server
            }
            dataToSend= {
                patient_id: this.props.user_id,
                morning: {
                    main: {},
                    dessert: {},                        
                },
                lunch: {
                    main: {},
                    dessert: {},                        
                },
                dinner: {
                    main: this.getSelectedFood(this.props.dinner.main),
                    dessert: this.getSelectedFood(this.props.dinner.dessert),
                },
            }

            this.props.amountCalculateEvening(dataToSend, this.state.mainMeal);
            
        }
    }

    getSelectedFood(listOfFood) {
        selected = []
        Array.from(listOfFood).map(el => {
            if (el.selected)
                selected.push(el.id);
        });
        return selected;

    }

    onMainBtnPress() {
        this.setState({
            displayMainMeal: true,            
        });
    }

    onDesertBtnPress() {
        this.setState({
            displayMainMeal: false,            
        });
    }


    renderWarning(){
        if (this.state.isVisible)
        return(
        <Overlay
            isVisible={this.state.isVisible}
            onBackdropPress={() => this.setState({ isVisible: false })}
            width="auto"
            height={100}
            overlayStyle={{alignSelf: "center", justifyContent: 'center',}}
            
        >
        <Text>Please select at least one food on the main menu</Text>
        </Overlay>
        );
    }


    iSMainBtnPressed() {
        // console.log(this.state.displayMainMeal);
        if (this.state.displayMainMeal) {
            // console.log(this.state.mainMeal);
            switch (this.state.mainMeal) {
                case BREAKFAST:
                    // console.log("choose breakfast");
                    food_data = this.props.morning.main;
                    break;
                case LUNCH:
                    // console.log("choose lunch");
                    food_data = this.props.lunch.main;
                    break;
                case DINNER:
                    // console.log("choose dinner");
                    food_data = this.props.dinner.main;
                    break;
                default:
                    // console.log("choose default");
                    food_data = this.props.morning.main;                    
            }

            if (typeof food_data !== 'undefined' && food_data.length > 0) {
                return(
                    <FlatList style={{width: "80%", flex: 4}}
                        data={food_data}  
                        renderItem={this._renderItem}
                        keyExtractor={item => item.id.toString()}
                        // extraData={food_data}
                    />
                );
            } else {
                return (
                    <View></View>
                )
            }


        } else {
            switch (this.state.mainMeal) {
                case BREAKFAST:
                    // console.log("choose breakfast");
                    dessert = this.props.morning.dessert;
                    break;
                case LUNCH:
                    // console.log("choose lunch");
                    dessert = this.props.lunch.dessert;
                    break;
                case DINNER:
                    // console.log("choose dinner");
                    dessert = this.props.dinner.dessert;
                    break;
                default:
                    // console.log("choose default");
                    dessert = this.props.morning.dessert;                    
            }

            return(
                <FlatList style={{width: "80%", flex: 4}}
                    data={dessert}  
                    renderItem={this._renderItem}
                    keyExtractor={item => item.id.toString()}
                />
            );
        }
        
    }

    resetMenu() {
        this.setState({
            mainMeal: BREAKFAST, // could be  breakfast, lunch, dinner
            displayMainMeal: true,
            btn_label: LUNCH_BTN_LABEL,
            navTitle: MORNING_NAV_TITLE,
        });
        this.props.resetSelection();
        this.props.foodFetch(this.props.token, this.props.user_id);
    }

    renderResetButton() {
        if (this.state.btn_label == FINISH_BTN_LABEL) {
            return(
                <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, marginTop: 0}}>
                    <Button title="Plan another menu" style={{width: "100%"}} onPress={this.resetMenu.bind(this)}></Button>
                </View>
            );
        } else {
            <View></View>
        }
    }

    nextBtn() {
        switch (this.state.mainMeal) {
            case BREAKFAST:
                this.setState({
                    navTitle: LUNCH_NAV_TITLE,
                    mainMeal: LUNCH,
                    btn_label: DINNER_BTN_LABEL,
                    
                });
                break;
            case LUNCH:
                this.setState({
                    navTitle: DINNER_NAV_TITLE,
                    mainMeal: DINNER,
                    btn_label: FINISH_BTN_LABEL,
                });        
                break;
            default:
                this.setState({
                    navTitle: DINNER_NAV_TITLE,
                    mainMeal: DINNER,
                    btn_label: FINISH_BTN_LABEL,
                });  

        }

    }

    prevBtn() {
        switch (this.state.mainMeal) {
            // case BREAKFAST:
            //     this.setState({
            //         navTitle: LUNCH_NAV_TITLE,
            //         mainMeal: LUNCH,
            //         btn_label: DINNER_BTN_LABEL,
                    
            //     });
            //     break;
            case LUNCH:
                this.setState({
                    navTitle: MORNING_NAV_TITLE,
                    mainMeal: BREAKFAST,
                    btn_label: LUNCH_BTN_LABEL,
                });        
                break;
            case DINNER:
                this.setState({
                    navTitle: LUNCH_NAV_TITLE,
                    mainMeal: LUNCH,
                    btn_label: DINNER_BTN_LABEL,
                    
                });
                break;
            default:
                this.setState({
                    navTitle: MORNING_NAV_TITLE,
                    mainMeal: BREAKFAST,
                    btn_label: LUNCH_BTN_LABEL,
                });        

        }

    } 

    render() {
        return(
            <View style={styles.container}>        
                <View style={{backgroundColor: "orange", width: "100%"}}>
                    <NavBar title={this.state.navTitle} ></NavBar> 
                </View>
                 {/* // body */}
                <View style={{flexDirection: "row", width: "80%", alignItems: "center", backgroundColor:"white", marginBottom: 0, marginTop: 5
                              }}>
                    <View style={{width: "50%", borderWidth: 1, borderColor: EMERALD}}>
                        <Button title="Main"  titleStyle={{color: EMERALD}} type="clear" onPress={this.onMainBtnPress.bind(this)} /> 
                    </View>
                    <View style={{width: "50%", borderWidth: 1, borderColor: EMERALD}}>
                        <Button title="Dessert"  titleStyle={{color: EMERALD}} type="clear" onPress={this.onDesertBtnPress.bind(this)} /> 
                    </View>
                    
                                        
                </View>

                {this.iSMainBtnPressed()}

            
                <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, marginTop: 0}}>
                    <Button
                        loading={this.props.calculating}                 
                        buttonStyle={styles.button} 
                        title={this.state.btn_label}
                        titleStyle={{color: 'white'}}
                        onPress={this.onButtonPress.bind(this)}
                    />                    
                </View>
                
                <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, marginTop: 0, flexDirection: "row"}}>
                    {/*  */}
                    <View style={{width: "50%", borderWidth: 1, borderColor: EMERALD, marginTop: 0}}>
                        <Button title="Prev" style={{width: "100%"}} onPress={this.prevBtn.bind(this)}></Button>                    
                    </View>
                    <View style={{width: "50%", borderWidth: 1, borderColor: EMERALD, marginTop: 0}}>
                        <Button title="Next" style={{width: "100%"}} onPress={this.nextBtn.bind(this)}></Button>
                    </View>                                        
                </View>

                {/* <View style={{width: "80%", borderWidth: 1, borderColor: EMERALD, marginTop: 0}}>
                    <Button title="Next" style={{width: "100%"}} onPress={this.nextBtn.bind(this)}></Button>
                </View> */}

                {this.renderResetButton()}
                {/* {this.renderWarning()} */}
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',        
        alignItems: "center",
        backgroundColor: "white",
    },
    button: {
        // backgroundColor: EMERALD, 
        width: "100%",
    }
});

const mapStateToProps = ( { food, auth, account } ) => {        
    token = auth.token;
    user_id =  auth.user_id || account.user_id;
    foodList = food.foodList;
    selectedFood = food.selectedFood;
    const { morning, lunch, dinner } = food.foodList;

    const { plannedMenu, calculating } = food;
    // console.log(plannedMenu);
    console.log(user_id);
    return {  selectedFood, token, morning, lunch, dinner, user_id, plannedMenu, calculating };
}

export default connect(mapStateToProps, { foodFetch, amountCalculate, resetSelection, amountCalculateMorning, amountCalculateAfternoon, amountCalculateEvening } )(FoodList);
