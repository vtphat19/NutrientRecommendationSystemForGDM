const SERVER_URL = "https://nutrient-recommendation.herokuapp.com";
// the IP 10.0.2.2 is the IP the Android simulator uses to communicate with the localhost 
const LOCAL_URL = "http://10.0.2.2:8000";
// use this url when want to run the the app in the physical android device, 
// given the server is run at localhost, port 8000, run this command to map the correct IP: adb reverse tcp:8000 tcp:8000
// LOCAL_URL = "http://127.0.0.1:8000/" // 

// change this to server for connnect the remote server or the local server
const RUNNING_URL = LOCAL_URL

export { SERVER_URL, LOCAL_URL, RUNNING_URL }
