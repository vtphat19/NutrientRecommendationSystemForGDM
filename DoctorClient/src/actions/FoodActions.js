import axios from '../components/axios';



import { FOOD_FETCH_SUCCESS, FOOD_SELECT, AMOUNT_CALCULATE, RESET_SELECTION, AMOUNT_CALCULATING } from './types';
import { Actions } from 'react-native-router-flux';


export const foodFetch = (token, user_id) => {
  console.log('in food fetch: ', user_id);
    const payload = {
      patient_id: user_id
    }

    const config = {
      headers: {
        Authorization: "Bearer " + token,
      }
    };

    return (dispatch) => {
        axios
          .post('api/food/', payload, config)
          .then(response => {
            // console.log(response);
            dispatch({ type: FOOD_FETCH_SUCCESS, payload: response.data });
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
};
  

export const foodSelect = (toggledData) => {
  return ({
    type: FOOD_SELECT,
    selected_food: toggledData
  });
};

export const amountCalculate = (dataToSend) => {
  // so far no data return, reducer does nothing

   const config = {
        headers: {
          Authorization: "Bearer " + token,
        }
    };

    return (dispatch) => {
        dispatch({ type: AMOUNT_CALCULATING });
        axios
          .post('calculator/morning', dataToSend, config)
          .then(response => {
            // console.log("response");
            // console.log(response);

            dispatch({ type: AMOUNT_CALCULATE, payload: response.data });
            Actions.planned_menu();
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
}

export const amountCalculateMorning = (dataToSend, mainMeal) => {
  // so far no data return, reducer does nothing

   const config = {
        headers: {
          Authorization: "Bearer " + token,
        }
    };

    return (dispatch) => {
        dispatch({ type: AMOUNT_CALCULATING, mainMeal: mainMeal });
        axios
          .post('calculator/morning', dataToSend, config)
          .then(response => {
            // console.log("response");
            // console.log(response);

            dispatch({ type: AMOUNT_CALCULATE, payload: response.data });
            Actions.planned_menu();
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
}


export const amountCalculateAfternoon = (dataToSend, mainMeal) => {
  // so far no data return, reducer does nothing

   const config = {
        headers: {
          Authorization: "Bearer " + token,
        }
    };

    return (dispatch) => {
        dispatch({ type: AMOUNT_CALCULATING, mainMeal: mainMeal });
        axios
          .post('calculator/afternoon', dataToSend, config)
          .then(response => {
            // console.log("response");
            // console.log(response);

            dispatch({ type: AMOUNT_CALCULATE, payload: response.data });
            Actions.planned_menu();
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
}

export const amountCalculateEvening = (dataToSend, mainMeal) => {
  // so far no data return, reducer does nothing

   const config = {
        headers: {
          Authorization: "Bearer " + token,
        }
    };

    return (dispatch) => {
        dispatch({ type: AMOUNT_CALCULATING, mainMeal: mainMeal });
        axios
          .post('calculator/evening', dataToSend, config)
          .then(response => {
            // console.log("response");
            // console.log(response);

            dispatch({ type: AMOUNT_CALCULATE, payload: response.data });
            Actions.planned_menu();
          })
          .catch(error => {
            console.log(error);
          });
            
          
    };
}

export const resetSelection = () => {
  return({
    type: RESET_SELECTION,
  });
}