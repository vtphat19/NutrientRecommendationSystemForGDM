import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import SignUpForm from './components/SignUpForm';
import FoodList from './components/FoodList';
import Profile from './components/Profile';
import PlannedMenu from './components/PlannedMenu';


const RouterComponent = () => {    
    return(
        <Router>
            <Scene key="root" hideNavBar={true} initial>
                <Scene key="auth">
                    <Scene key="login" component={LoginForm} hideNavBar={true}></Scene>
                    <Scene key="signup" component={SignUpForm} hideNavBar={true}></Scene>    
                </Scene>
                
                <Scene tabs key='main' labelStyle={{ fontSize: 16, marginBottom: 15 }}>                    
                         <Scene tabBarLabel="Food Menu" initial hideNavBar={true}>
                            <Scene component={FoodList} key="foodlist" title="Food Menu" initial titleStyle={{alignSelf: 'center', justifyContent: 'center'}}  ></Scene>
                         </Scene>
                         <Scene tabBarLabel="Planned Menu" hideNavBar={true}>
                            <Scene key="planned_menu" component={PlannedMenu} title="Food Menu" ></Scene>
                         </Scene>
                         <Scene tabBarLabel="Profile" hideNavBar={true} key='profile'>
                            <Scene component={Profile}  ></Scene>
                         </Scene>                         
                </Scene>
            </Scene>

        </Router>
    );
};

export default RouterComponent;


