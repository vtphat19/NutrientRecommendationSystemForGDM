# Overview
The project is currently hosted at https://nutrient-recommendation.herokuapp.com/

The APIs endpoint documentation is located at: 
- https://nutrient-recommendation.herokuapp.com/redoc/

# Installation Guide
This section is to give instructions to install the system. Firstly, user is required to download the source code which is maintained at [GitLab](https://gitlab.com/vtphat19/NutrientRecommendationSystemForGDM/) , a platform to manage source code. The source code is placed in a folder called ”NutrientRecommendationSystemForGDM”. In
the folder, there are two folder called ”server” and ”DoctorClient” which contains source
code of implemented back-end and front-end programs.

## Backend Installation Guide
### Requirements
The following programs are required to install the backend system
- Python 3. Python programming language version 3
- pip. A package manager for Python 3
- virtualenv. A Python package to create isolated enviroment
- PostgreSQL. A relational database management system

#### Server
The Python 3 can be downloaded in the [official Python homepage](https://www.python.org/downloads/
). The package installer pip is ussually installed when a Python version is installed. User can also manually install by using system package manager or download it at [here](https://bootstrap.pypa.io/get-pip.py
) . After Python and pip package manager is installed, user should setup a virtual environment to install Python libraries to build up the server. We can use the package virtualenv which can be installed by running the command:
```bash
> pip install virtualenv
# On Ubuntu, there might be two version pip and pip3 for Python 2, and Python 3. 
# Therefore, you might need to run this:
> pip3 install virtualenv
```
Then to create an isolated Python environment, we run the command
```bash
 > python -m virtualenv < my_env >
 # < my_env > is the name of the isolated enviroment
 # you can also run
 > virtualenv < my_env >

# On system like Ubuntu, python version 2 is often preinstalled along with python version 2. 
# As a result, you might need to run this command to ensure creating a virtual enviroment with Python version 3
> python3 -m virtualenv < my_env >
```
At your current working directory, the above command will create a folder with the name of
created environment. Then you need to activate the environment, by running the command:
```bash
 # On Windows operating system , we can run
 > \path\to<my_env>\ Scripts\activate
 # On Linux and MacOS operating system , we can run
 > source/path/to/< my_env >/bin/activate
```
If you activate the virtual enviroment successfully, the name of that enviroment should appear in your terminal (or Windows command prompt).

Move your current working directory to server directory, then run the following command
to install all the libraries for the server which is listed in the requirement.txt file.
```bash
> pip install -r requirement.txt
```

#### Database
Assume you have not preinstalled PostgreSQL in your system, please read this section. PostgreSQL can be downloaded from its [website](https://www.postgresql.org/download/). For Windows users, please go to the website and download the executable installer. Then run the wizard. For Ubuntu users, you can use the system package manager like apt to install PostgreSQL. Please go to PostgreSQL for see suitable instructions for you system.
Assume PostgreSQL is installed correctly, we need to configure to make it run correctly with the server. In this guide, we will use the user postgres to create a dababase. 

The user postgres is created during installing PostgreSQL. For Windows users, the password of that postgres is asked during installation step of PostgreSQL. For Ubuntu users, we need to create password manually after installing PostgreSQL. To do that, we can run the following commands:
```bash
# Firstly, using the super user, login as postgres user,
#  and run the utility psql with postgres user
sudo -u postgres psql postgres
# Then you should see the new prompt
postgres=# 
# Then run
postgres=# \password postgres
# then system will prompt you to enter password for user posgres
# please enter and remember it
```
Now we need to create a database, make sure you are still logging as postgres user
```bash
 # On Ubuntu system, in the terminal,
 > sudo -u postgres psql postgres 
 # On Windows, in the command prompt,
 > psql -U postgres # then enter your password of postgres
```
Create a new database,
```bash
 # On Ubuntu system
 > createdb < my_db >
 # On Windows
 > CREATE < my_db >
 # < my_db > is the name of the database
```
Then open the file .env in the sever/server/settings folder, and edit the value of DB NAME, DB USER and DB PASSWORD entries to your database name, postgres and password of the user.

### Setup Django server
There are two settings in the implementation: development and staging. While the development setting is to run the Django server locally. the staging setting is for production. The main differences are about the value of environment variables in the running system. This section guides how to run Django server locally. This section assume that you have already activated the virtual enviroment and run all above steps successfully.

Every time booting up the server, it reads the environment variable: STAGE. You need to export in your system.
```bash
# For Ubuntu,
> export STAGE='DEV'
# to check it you can run,
> echo $STAGE
# then you should see 'DEV' in the terminal

# For Windows,
> set STAGE='DEV'
# you can check it with
> set STAGE
```
At the server folder, we need to create a superuser that is actually the admistrator user in the system. 
```bash
> python manage.py createsuperuser
# Then you should enter information to create the user
```
We also need to create tables in the database, 
```bash
# Make migration file 
> python manage.py makemigrations
# Take migration into effect
> python manage.py migrate
```
Now you can start the server, by running
```bash
> python manage runserver
```
Go to the browser, enter the address: http://127.0.0.1:8000/admin. Enter your previous superuser email and password. Then you should enter the Django admin page. For the newly installation, you should create data by interact with the admin dashboard.

## Frontend Installation Guide
### Requirements
You should install to work with the frontend development.
- Node.js
- React native
- Android Studio

Node.js can be download from its [website](https://nodejs.org/en/download). Please go to the website and download the suitable version with your system.

React Native can be installed with the command:
```bash
# On both Windows and Linux,
> npm install -g react-native-cli
```
Android Studio can be installed by download at [here](https://developer.android.com/studio/). The details of instruction can be found at [here](https://developer.android.com/studio/install).

After Android Studio is installed successfully, we need Android 9 (Pie) SDK. The instruction to install it is placed at [here](https://facebook.github.io/react-native/docs/getting-started.html).
After install the SDK, you need to setup environment variables. 
For the Ubuntu users,  add the following lines to your $HOME/.bash_profile or $HOME/.bashrc config file:
```bash
export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
```
Then you can update the changes with the command:
```bash
> source $HOME/.bash_profile
```

Optionally you can install Watchman, since it is recommended for development. The details of installtion is at [here](https://facebook.github.io/watchman/docs/install.html#system-requirements).


Finnaly, you need to install dependencies for the application. At the DoctorClient folder, run
```bash
> npm install
```

### Running React Native application
To run the React native application, please move to the DoctorClient directory. Then run the command
```bash
> npm run start
```
The command only start a server for compiling code. For injecting the application, you need to plug an Android smartphones with your machinee or open Android simulator. Then you should run the command:
```bash
> react-native run-android
```
Then it will build and install an Android application into your smartphones or simulator.
Note: While react native is able to build apps for iOS as well. Since I do not have Mac devices, I have not tested this functionality.

The client app has a setting file to where the server is to communicate. The setting is at 'NutrientRecommendationSystemForGDM/DoctorClient/src/services/network.js'. To let the local server communicates with the Android simulator, you need to have setting: 
```javascript
const LOCAL_URL = "http://10.0.2.2:8000";
```
But if you testing with a physical Android smartphone, you need to change to
```javascript
LOCAL_URL = "http://127.0.0.1:8000/" // 
```
The remote server URL is marked as
```javascript
const SERVER_URL = "https://nutrient-recommendation.herokuapp.com";
```
For your purpose, please change the constant RUNNING_URL
```javascript
const RUNNING_URL = LOCAL_URL 
// or 
const RUNNING_URL = SERVER_URL 
```

#### Appendix: Export Database relation: (Devepoment enviroment):
I used a library called pygraphviz to export the database diagram.
To install it in your enviroment, 
In the terminal, run the the following command to install the dependencies:
```bash
    sudo apt-get install graphviz libgraphviz-dev pkg-config
```    

To export the database diagram, the command below can be used 
```bash                    
    python manage.py graph_models -a -g -o database-diagram.png
```
