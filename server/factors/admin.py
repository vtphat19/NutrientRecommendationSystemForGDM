from django.contrib import admin
from .models import Activity, StressFactor
# Register your models here.

class ActivityAdmin(admin.ModelAdmin):
    list_display = ['description', 'score']

class StressFactorAdmin(admin.ModelAdmin):
    list_display = ['description', 'score']

admin.site.register(Activity, ActivityAdmin)

admin.site.register(StressFactor, StressFactorAdmin)