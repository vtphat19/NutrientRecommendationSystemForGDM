from django.db import models

# Create your models here.
class Activity(models.Model):
    description = models.CharField(max_length=255)
    score = models.FloatField(unique=True, default=0.0)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'

class StressFactor(models.Model):
    description = models.CharField(max_length=255)
    score = models.FloatField(unique=True, default=0.0, null=False)
    def __str__(self):
        return self.description

    class Meta:
        verbose_name = 'Stress Factor'
        verbose_name_plural = 'Stress Factors'