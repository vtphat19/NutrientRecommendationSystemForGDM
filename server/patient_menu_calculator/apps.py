from django.apps import AppConfig


class PatientMenuCalculatorConfig(AppConfig):
    name = 'patient_menu_calculator'
