from rest_framework import serializers
from .models import Food, DataSource

from users.models import FoodForPatient

class DataSourceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DataSource
        fields = ['name']

class FoodSerializer(serializers.HyperlinkedModelSerializer):
    tags = serializers.SerializerMethodField()
    
    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('tags')

    def get_tags(self, obj):
        return u", ".join(o.name for o in obj.tags.all())

    class Meta:
        model = Food
        fields = [
            'id', 'name', 'description', 'image',
            'ingredient', 'date_modified', 'date_created',
            'manufacturer', 'data_source', 'group', 'sub_group', 'tags'
        ]

from django.conf import settings
class FoodForPatientSerializer(serializers.Serializer):    
    def get_name(self, obj):
        print(obj.portion.food_id)
        
        return str(obj.portion.food_id)
    
    def get_image(self, obj):        
        return settings.MEDIA_URL +  str(obj.portion.food_id.image)

    def get_id(self, obj):
        return obj.portion.id

    id = serializers.SerializerMethodField() # return the id of the portion, not tge id of a FoodForPatient row
    name = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    portion = serializers.CharField()
    tag     = serializers.CharField()


        
