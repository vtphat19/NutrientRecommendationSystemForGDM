from django.contrib import admin
from django import forms
from taggit.managers import TaggableManager # third party tag model
from .models import Measure_Unit, Nutrient, \
                    Portion, \
                    Ingredient, Food, \
                    DataSource, Manufacturer,  NutrientList



class NutrientAdmin(admin.ModelAdmin):
    pass

class PortionInline(admin.TabularInline):
    model = Portion
    # max_num = 1
    extra = 0
    show_change_link = True
    

class NutrientInline(admin.TabularInline):
    model = Nutrient
    extra = 0
    # fk_name = 
    # show_change_link = True

class PortionAdmin(admin.ModelAdmin):
    inlines = [
        NutrientInline,
    ]
    list_display = ('id','name', 'food_id', 'get_group', 'get_sub_group')

class FoodGroupForm(forms.ModelForm):
    GROUP_CHOICES = (
        ('Carbohydrate Group', 'Carbohydrate Group'),
        ('Protein Group', 'Protein Group'),
        ('Fat Group', 'Fat Group'),
        (' ', ' ')
    )
    
    SUBGROUP_CHOICES = (
        ('Starch', 'Starch'),
        ('Fruit', 'Fruit'),
        ('Fat-Free, 1% (low-fat) (Milk & Milk Substitutes)', 'Fat-Free, 1% (low-fat)-Milk & Milk Substitutess'),
        ('2% (reduced-Fat) (Milk & Milk Substitutes)', '2% (reduced-Fat) (Milk & Milk Substitutes)'),
        ('Whole Milk (Milk & Milk Substitutes)', 'Whole Milk (Milk & Milk Substitutes)'),
        ('Non-Starchy Vegetables', 'Non-Starchy Vegetables'),
        ('Other Carbohydrates', 'Other Carbohydrates'),
        ('Lean Animal Protein', 'Lean Animal Protein'),
        ('Medium-Fat Animal Protein', 'Medium-Fat Animal Protein'),
        ('High-Fat Animal Protein', 'High-Fat Animal Protein'),
        ('Plant-Based Proteins', 'Plant-Based Proteins'),
        ('Fats & Oils', 'Fats & Oils'),
        (' ', ' ')
        
    )
    
    group = forms.ChoiceField(choices=GROUP_CHOICES)

    sub_group = forms.ChoiceField(choices=SUBGROUP_CHOICES)

class GroupFoodAdmin(admin.ModelAdmin):
    form = FoodGroupForm
    list_display = ('group', 'sub_group')



class FoodAdmin(admin.ModelAdmin):
    list_filter = (
        'tags',
        'group',
        'sub_group',
    )    
    search_fields = ['name']
    list_per_page = 15
    ordering = ('id',)
    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('tags')

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())

    inlines = [
        PortionInline,
    ]
    form = FoodGroupForm
    list_display = ('id','name','group', 'sub_group', 'tag_list')
class NutrientListForm(forms.ModelForm):
    

    # These main group of nutrients are taken from 
    # https://www1.health.gov.au/internet/publications/publishing.nsf/Content/canteen-mgr-tr1~nutrients
    # Please modify and update if necessary the list
    CHOICES = (
        ('Carbohydrates', 'Carbohydrates'),
        ('Proteins', 'Proteins'),
        ('Lipids (fats)', 'Lipids (fats)'),
        ('Proteins', 'Proteins'),
        ('Vitamins', 'Vitamins'),
        ('Minerals', 'Minerals'),
        ('Water', 'Water'),
        ('Other', 'Other')
    )
    
    category = forms.ChoiceField(choices=CHOICES)


class NutrientListAdmin(admin.ModelAdmin):
    form =  NutrientListForm
    list_display = ('nutrient_name', 'category', 'ordered_no')
    
    # def nutrient_name(self, obj):
    #     return obj.nutrient_name
    # nutrient_name.admin_order_field = 'ordered_no'
    
    ordering = ('ordered_no',)

admin.site.register(Measure_Unit)
# Nutrient tabale is not displayed because it is not neccessary
# admin.site.register(Nutrient, NutrientAdmin)
admin.site.register(NutrientList, NutrientListAdmin)

admin.site.register(Portion, PortionAdmin)
admin.site.register(Ingredient)
admin.site.register(Food, FoodAdmin)
admin.site.register(DataSource)
admin.site.register(Manufacturer)

# from taggit.models import Tag
# admin.site.register(Tag)