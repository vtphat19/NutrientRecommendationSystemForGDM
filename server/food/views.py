from .models import Food, DataSource
from rest_framework import viewsets
from .serializers import FoodSerializer, DataSourceSerializer, FoodForPatientSerializer

from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# Create your views here.

class DataSourceViewSet(viewsets.ModelViewSet):
    queryset = DataSource.objects.all() 
    serializer_class = DataSourceSerializer

    
# class FoodViewSet(viewsets.ViewSet):
#     def list(self, request):
#         queryset = Food.objects.all()
#         serializer = FoodSerializer(queryset,  many=True, context={'request': request})
#         return Response(serializer.data)
    
    
class FoodViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated,]
    queryset = Food.objects.all()
    serializer_class = FoodSerializer
        

from users.models import FoodForPatient
from rest_framework.decorators import api_view

@api_view(['POST'])
def food_for_patient(request):
    if  request.method == 'POST':    
        # print(request.data)
        queryset = FoodForPatient.objects.filter(patient=request.data["patient_id"])

        serializer = FoodForPatientSerializer(queryset, context={'request': request}, many=True)
        print(queryset)
        return Response(serializer.data)
    
