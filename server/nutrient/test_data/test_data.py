# data = {

#     4:{
#     'patient_id': '42', 
#     'morning': {
#         'main': [1, 2], # cơm,  fried chicken
#         'dessert': [9]}, #banana
#     'lunch': {
#         'main': [1, 15, 12], # rice, vegetables, Spare ribs (Xườn heo)
#         'dessert': [19]}, #yogurt 
#     'dinner': {
#         'main': [1, 15, 12], 
#         'dessert': [19]} # yogurt   
#     },
    
# }


data = {
    0: {
    'patient_id': '42', 
    'morning': {
        'main': [25], # phở 
        'dessert': [9]}, 
    'lunch': {
        'main': [14, 16], # cá chiên, cơm lứt
        'dessert': [26]}, 
    'dinner': {
        'main': [22, 12, 1], # gà rô ti, rau, cơm trắng 
        'dessert': [9]} # banana
    }, 
    1:{
    'patient_id': '42', 
    'morning': {
        'main': [1, 15], # cơm trắng
        'dessert': [9]}, 
    'lunch': {
        'main': [14, 16], # cá chiên, cơm lứt
        'dessert': [26]}, 
    'dinner': {
        'main': [22, 12, 1], # gà rô ti, rau, cơm trắng 
        'dessert': [9]} # banana    
    },
    2:{
    'patient_id': '42', 
    'morning': {
        'main': [1, 17], # cơm trắng, trứng
        'dessert': [26]}, 
    'lunch': {
        'main': [14, 16], # cá chiên, cơm lứt
        'dessert': []}, 
    'dinner': {
        'main': [22, 12, 1], # gà rô ti, rau, cơm trắng 
        'dessert': [9]} # banana    
    },
    3:{
    'patient_id': '42', 
    'morning': {
        'main': [1, 2], # cơm,  2 
        'dessert': [26]}, 
    'lunch': {
        'main': [14, 16], # cá chiên, cơm lứt
        'dessert': [26]}, 
    'dinner': {
        'main': [22, 12, 1], # gà rô ti, rau, cơm trắng 
        'dessert': [9]} # banana    
    },
    4:{
    'patient_id': '42', 
    'morning': {
        'main': [1, 2], # cơm,  fried chicken
        'dessert': [9]}, #banana
    'lunch': {
        'main': [1, 15, 12], # rice, vegetables, Spare ribs (Xườn heo)
        'dessert': [19]}, #yogurt 
    'dinner': {
        'main': [1, 15, 12], 
        'dessert': [19]} # yogurt   
    },
    5:
        {'patient_id': '42', 'morning': {'main': [1, 17], 'dessert': [19,26]}, 'lunch': {'main': [18, 1, 12, 24], 'dessert': [26]}, 'dinner': {'main': [15, 24, 1], 'dessert': [26]}}

    
}
