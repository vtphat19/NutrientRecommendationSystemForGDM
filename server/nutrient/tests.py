# from django.test import TestCase
import unittest


from .serializers import SelectedFoodSerializer

from users.models import Patient, FoodForPatient
from food.models import Nutrient
from .calculator import Calculator

from .views import input_ga
from .test_data.test_data import data

import time

# Create your tests here.
class TestGenAlg(unittest.TestCase):
    def test_output(self):
        for d in range(len(data)): 
            serializer = SelectedFoodSerializer(data=data[d])
            selected_food = []
            print(serializer.is_valid())
            if serializer.is_valid() == True:            
                patient = Patient.objects.get(pk=serializer.data['patient_id'])
                print(patient)
                for f in serializer.data['morning']['main']:
                    
                    # f_qs = FoodForPatient.objects.get(portion=f, tag__name='sáng', patient=patient)
                    f_qs = input_ga(portion=f, tag='sáng', patient=patient)
                    # print(f_qs )
                    selected_food.append(f_qs)
                
                for f in serializer.data['morning']['dessert']:
                    # print(f)
                    # print(FoodForPatient.objects.filter(portion=f, patient=patient))
                    # f_qs = FoodForPatient.objects.get(portion=f, tag__name='dessert', patient=patient)
                    f_qs = input_ga(portion=f, tag='dessert', patient=patient)
                    selected_food.append(f_qs)

                for f in serializer.data['lunch']['main']:
                    
                    f_qs = input_ga(portion=f, tag='trưa', patient=patient)
                    # f_qs = FoodForPatient.objects.get(portion=f, tag__name='trưa', patient=patient)
                    selected_food.append(f_qs)
                for f in serializer.data['lunch']['dessert']:
                    f_qs = input_ga(portion=f, tag='dessert', patient=patient)
                    # f_qs = FoodForPatient.objects.get(portion=f, tag__name='dessert', patient=patient)
                    selected_food.append(f_qs)

                for f in serializer.data['dinner']['main']:
                    f_qs = input_ga(portion=f, tag='tối', patient=patient)
                    # f_qs = FoodForPatient.objects.get(portion=f, tag__name='tối', patient=patient)
                    selected_food.append(f_qs)
                for f in serializer.data['dinner']['dessert']:
                    # f_qs = FoodForPatient.objects.get(portion=f, tag__name='dessert', patient=patient)
                    f_qs = input_ga(portion=f, tag='dessert', patient=patient)
                    selected_food.append(f_qs)                
                print("selected_food")
                for s in selected_food:
                    print(s)
                print('start calling ga')
                            
                start_time = time.time()
                
                
                ga_calculator = Calculator(selected_food, patient.carbohydrate, patient.protein, patient.lipid, patient.energy)
                pop, hall_of_fame = ga_calculator.ga()


                print("--- %s seconds ---" % (time.time() - start_time))
                print('end calling ga')

