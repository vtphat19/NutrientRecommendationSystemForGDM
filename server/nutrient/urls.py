from .views import calculate_needed_amount_intake, calculate_needed_amount_morning
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'calculator', calculate_needed_amount_intake, basename='calculate_needed_amount_intake')
router.register(r'calculator/morning', calculate_needed_amount_morning basename='calculate_needed_amount_morning')
urlpatterns = router.urls