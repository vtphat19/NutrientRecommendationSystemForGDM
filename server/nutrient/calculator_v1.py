'''
This section to prepare tools for the GA algorithm
'''

# Individual
import random
import array
from deap import base
from deap import creator
from deap import tools
import numpy 
from deap.tools import HallOfFame
from statistics import mean
from food.models import Nutrient
import matplotlib.pyplot as plt
import operator
import sys
import math
import logging

'''Helper function to get the lower bound and upper bound of individuals'''
def getBounds(selected_food):
    lower_bounds = []
    upper_bounds  = []

    for s in selected_food:
        # print(s)
        # print(s, s.lower_bound, s.upper_bound)
        lower_bounds.append(s.lower_bound)
        upper_bounds.append(s.upper_bound)
        
    return lower_bounds, upper_bounds

class Calculator():
    def initIndividual(self, icls):
        indGenerator = []

        for s in self.selected_food:        
            indGenerator.append(random.randint(s.lower_bound, s.upper_bound))
                    
        return icls(indGenerator)


    def testResult(self, individual):
        p = 0
        l = 0
        c = 0
        e = 0    
        print('call test result func')
        print("Patient need %s protein, %s lipid, %s carb" %(self.P, self.L, self.C))
        print("Total energy expendire he needs: %s" %(self.E)) #%(self.P*4+self.L*9+self.C*4))
        print(individual)        
        for f in range(len(self.selected_food)):
            
            protein = self.selected_food[f].protein
            lipid = self.selected_food[f].lipid
            carb = self.selected_food[f].carb
            energy = self.selected_food[f].energy
            print("%s has %s protein, %s lipid, %s carb. Total: %s calo" %(self.selected_food[f], protein, lipid, carb, energy))
            p = p + protein * individual[f]
            l = l + lipid * individual[f]
            c = c + carb * individual[f]  
        print("%s protein, %s lipid, %s carb" %(p, l, c))
        print("Total energy from planned menu: %s" %(p*4+l*9+c*4))
        
        e = p*4+l*9+c*4 - self.E
        p = p - self.P
        l = l - self.L
        c = c - self.C    
        
        print('diff: %s' %(max(abs(p), abs(l), abs(c), abs(e))))
        print()

    def getNutrient(self, individual):
        p = 0
        l = 0
        c = 0
        e = 0    

        for f in range(len(self.selected_food)):
            
            protein = self.selected_food[f].protein
            lipid = self.selected_food[f].lipid
            carb = self.selected_food[f].carb
            energy = self.selected_food[f].energy

            # print('%s has %s protein, %s lipid, %s carb, %ss energy' %(self.selected_food[f], protein, lipid, carb, energy))


            p = p + protein * individual[f]
            l = l + lipid * individual[f]
            c = c + carb * individual[f]  
        # print("%s protein, %s lipid, %s carb" %(p, l, c))
        # print("Total energy from planned menu: %s" %(p*4+l*9+c*4))
        
        e = p*4+l*9+c*4 
        # return e, p, l, c
        return {"energy": e, "protein": p, "lipid": l, "carb": c}


    '''Evaluate fitness of an individual '''
    def evaluate(self, individual):
        p = 0
        l = 0
        c = 0    
        e = 0

        for f in range(len(self.selected_food)):
            p = p + self.selected_food[f].protein * individual[f]
            l = l + self.selected_food[f].lipid * individual[f]
            c = c + self.selected_food[f].carb * individual[f]
            e = e + self.selected_food[f].energy * individual[f]

        p = p - self.P
        l = l - self.L
        c = c - self.C    
        e = e - self.E

        return max(abs(p), abs(l), abs(c), abs(e)), 

    '''Feasibility function for the individual. Returns True if feasible False otherwise.'''
    
    def evalEnergy(self, individual, selected_food):
        # Evaluate the fitness value of the individual 
        p = 0
        l = 0
        c = 0    
        for f in range(len(selected_food)):
            p = p + selected_food[f].protein * individual[f]
            l = l + selected_food[f].lipid * individual[f]
            c = c + selected_food[f].carb * individual[f]  
        return p*4 + l*9 + c*4 
    
    def getDistance(self, ind):
    #     print("target: ", target)
        return math.sqrt((self.E - ind["energy"])**2 + (self.P - ind["protein"])**2 + (self.L - ind["lipid"])**2 + (self.C - ind["carb"])**2)


    def feasible(self, individual):        
        '''
        Only the carbohydrates of all the food are considered. As the diabetes need to restrict themselves from carbohydrates
        '''
        c = 0
        p = 0
        l = 0
        e = 0
        for f in range(len(self.selected_food)):                
            c = c + self.selected_food[f].carb * individual[f] 
            p = p + self.selected_food[f].protein * individual[f] 
            l = l + self.selected_food[f].lipid * individual[f] 
            e = e + self.selected_food[f].energy * individual[f]
        if (self.E-100 < e < self.E+100) and (self.C-30 < c < self.C+30) and (self.P -30 < p < self.P + 30) and (self.L - 30 < l < self.L + 30):        
        # if (-0.1 < abs(e-self.E)/self.E < 0.1) and (-0.1 < abs(c-self.C)/self.C < 0.1) and (-0.1 < abs(p-self.P)/self.P < 0.1) and (-0.1 < abs(l-self.L) < 0.1 ):        
            return True       
        return False

    def __init__(self, selected_food, carbohydrate=0, protein=0, lipid=0, energy=0):
        ''' Initialize some parameters to use later'''
        # IND_SIZE is the size of an individual
        IND_SIZE = len(selected_food) 

        self.selected_food = selected_food
        self.toolbox = base.Toolbox()

        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        
        # using array.array makes the program run faster!
        # read this the section Performance for explaination by authors of DEAP library
        # https://deap.readthedocs.io/en/master/tutorials/advanced/numpy.html?highlight=array.array#performance
        creator.create("Individual", array.array, typecode="d", fitness=creator.FitnessMin) 
        
        self.lower_bounds, self.upper_bounds = getBounds(selected_food)

        self.P = protein
        self.L = lipid
        self.C = carbohydrate        
        self.E = energy
        
        '''
        Register the tools
        '''
        self.toolbox.register("individual", self.initIndividual, creator.Individual)        
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)
        self.toolbox.register("mate", tools.cxTwoPoint)
        self.toolbox.register("mutate", tools.mutUniformInt, low=self.lower_bounds, up=self.upper_bounds, indpb=0.5)
        self.toolbox.register("select", tools.selTournament, tournsize=10)
        self.toolbox.register("evaluate", self.evaluate)

        '''        
        # Documentation the Penalty Function
        # https://deap.readthedocs.io/en/master/tutorials/advanced/constraints.html
        '''
        self.toolbox.decorate("evaluate", tools.DeltaPenalty(feasibility=self.feasible, delta=mean([self.P,self.L, self.C, self.E]))) 

    def checkValiditiy(self, ind):
        # print(ind)
        for i in range(len(ind)):
            if  not(self.lower_bounds[i] < ind[i] < self.upper_bounds[i]):
                return False
            # print(ind[i])
        # print('x')
        return True

    def ga(self):
        pop = self.toolbox.population(n=500) # 100
        CXPB, MUTPB, NGEN = 0.1, 0.05, 100 # 100
        halloffame = HallOfFame(maxsize=10)
        # Evaluate the entire population
        # fbest = numpy.ndarray((NGEN,1))
        fbest = numpy.zeros((NGEN))
        
        fitnesses = map(self.toolbox.evaluate, pop)
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit

        g = 0 
        while True:
            g += 1
            # print('g= ', g)
            # Select the next generation individuals
            offspring = self.toolbox.select(pop, len(pop))
            # Clone the selected individuals
            offspring = list(map(self.toolbox.clone, offspring))
            
            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < CXPB:
                    self.toolbox.mate(child1, child2)
                    ## check validity of offspring after crossover
                    ## remove offspring if it violates the boundaries
     
                    # if (self.checkValiditiy(child1) == False):
                    #     offspring.remove(child1)
                    # if (self.checkValiditiy(child2) == False):
                    #     offspring.remove(child2)                        

                    del child1.fitness.values
                    del child2.fitness.values
            # Mutate the offspring                
            for mutant in offspring:
                if random.random() < MUTPB:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values
            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit
            # The population is entirely replaced by the offspring        
            pop[:] = offspring
            halloffame.update(pop)

            closestDis = sys.maxsize
            lastBest = None
            found = False
            for h in halloffame:        
                nutri = self.getNutrient(h)
#                 print('nutri: ')
#                 print(nutri)
#                 dis = getDistance(target[i], nutri)
                dis = self.getDistance(nutri)
#                 print('dis: , closestDis: ', dis, closestDis)
                if dis < closestDis:                    
                    closestDis = dis
                    bestInd = h
                    bestNutri = nutri
            if lastBest != bestInd:
                lastBest = bestInd
            elif g > 1500:
                logging.info('same ind as best: ', lastBest)
                logging.info('it has: ', bestNutri)
                logging.info('current is ', bestInd)
                logging.info('it has: ', bestNutri)
                # found = False
                break

            if g > 1500:                
                logging.info('current is ', bestInd)
                logging.info('it has: ', bestNutri)
                # found = False
                break

            if self.feasible(bestInd):
                logging.info('found the best: ', bestInd)
                logging.info('it has: ', bestNutri)
                logging.info('stop at g= ', g)
                found = True
                break

            # sorted(halloffame,1 key=operator.attrgetter())
    
            # fbest[g] = self.evalEnergy(halloffame[0], self.selected_food)


    
        # for h in halloffame:
        #     # print(h)
        #     self.testResult(h)



        # x = list(range(NGEN))
        # plt.figure()    
        # plt.semilogy(x, fbest, "-c")
        # plt.grid(True)
        # plt.title("Fitness value")
            
        # plt.show()

        # print energy provided by our best individuals
        # print(fbest)

        return pop, halloffame, bestInd, bestNutri, found




        