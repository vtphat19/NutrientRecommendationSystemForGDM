from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from .serializers import SelectedFoodSerializer
from users.models import Patient, FoodForPatient
from food.models import Nutrient
from .calculator import Calculator

import time


class input_ga():
    def  __repr__(self):
        return str(self.food_for_patient)
    
    def __init__(self, portion, tag, patient):
        self.food_for_patient = FoodForPatient.objects.get(portion=portion, tag__name=tag, patient=patient)
        self.food_nutrient = Nutrient.objects.filter(portion_id=portion)                        
        self.protein = self.food_nutrient.get(name_id='Protein').amount
        self.lipid    = self.food_nutrient.get(name_id='Total lipid (fat)').amount
        self.carb   = self.food_nutrient.get(name_id='Carbohydrate, by difference').amount
        self.lower_bound = self.food_for_patient.lower_bound
        self.upper_bound = self.food_for_patient.upper_bound
        self.energy  = self.food_nutrient.get(name_id='Energy').amount
        # print(self.food_for_patient)
        # print(self.food_nutrient)
        # print(self.protein)
        # print(self.carb)
        # print(self.lipid)
        # print(self.lower_bound)
        # print(self.upper_bound)

def clean_selectedFood(portion, tag_name, patient):
    f_qs = FoodForPatient.objects.get(portion=portion, tag__name=tag, patient=patient)    

@api_view(['POST'])
def calculate_needed_amount_intake(request):
    if request.method == 'POST':
        print("request.data")
        print(request.data)
        serializer = SelectedFoodSerializer(data=request.data)

        selected_food = []
        print(serializer.is_valid())
        # Not check validivity of format, be careful!
        if serializer.is_valid() == True or (serializer.is_valid() != True):            
            patient = Patient.objects.get(pk=serializer.data['patient_id'])
            print(patient)
            for f in serializer.data['morning']['main']:
                
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='sáng', patient=patient)
                f_qs = input_ga(portion=f, tag='sáng', patient=patient)
                # print(f_qs )
                selected_food.append(f_qs)
            
            for f in serializer.data['morning']['dessert']:
                # print(f)
                # print(FoodForPatient.objects.filter(portion=f, patient=patient))
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='dessert', patient=patient)
                f_qs = input_ga(portion=f, tag='dessert', patient=patient)
                selected_food.append(f_qs)

            print(selected_food)
            print('patient.mor %s' %(patient.morning_energy))
            ga_calculator = Calculator(selected_food, patient.morning_carbohydrate, patient.morning_protein, patient.morning_lipid, patient.morning_energy) # 30% of total energy
            pop, hall_of_fame = ga_calculator.ga()
            print('done morning')
            selected_food = []
            for f in serializer.data['lunch']['main']:
                f_qs = input_ga(portion=f, tag='trưa', patient=patient)
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='trưa', patient=patient)
                selected_food.append(f_qs)
            for f in serializer.data['lunch']['dessert']:
                f_qs = input_ga(portion=f, tag='dessert', patient=patient)
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='dessert', patient=patient)
                selected_food.append(f_qs)

            selected_food = []
            for f in serializer.data['dinner']['main']:
                f_qs = input_ga(portion=f, tag='tối', patient=patient)
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='tối', patient=patient)
                selected_food.append(f_qs)
            for f in serializer.data['dinner']['dessert']:
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='dessert', patient=patient)
                f_qs = input_ga(portion=f, tag='dessert', patient=patient)
                selected_food.append(f_qs)                
            ga_calculator = Calculator(selected_food, patient.carbohydrate, patient.protein, patient.lipid, patient.energy)
            pop, hall_of_fame = ga_calculator.ga()            
            # print("selected_food")
            # print(selected_food)
            # print('start calling ga')
                        
            # start_time = time.time()
            
            
            # print("--- %s seconds ---" % (time.time() - start_time))
            # print('end calling ga')

            # print(hall_of_fame)

        
            response_data = {
                "choice 1": hall_of_fame[0],
                "choice 2": hall_of_fame[1],
                "choice 3": hall_of_fame[2],
            }
            
            # print(response_data)
            # serializer.save()
            
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def calculate_needed_amount_morning(request):
    if request.method == 'POST':
        print("request.data")
        print(request.data)
        serializer = SelectedFoodSerializer(data=request.data)

        selected_food = []
        print(serializer.is_valid())
        # Not check validivity of format, be careful!
        if serializer.is_valid() == True or (serializer.is_valid() != True):            
            patient = Patient.objects.get(pk=serializer.data['patient_id'])
            print(patient)
            for f in serializer.data['morning']['main']:
                
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='sáng', patient=patient)
                f_qs = input_ga(portion=f, tag='sáng', patient=patient)
                # print(f_qs )
                selected_food.append(f_qs)
            
            for f in serializer.data['morning']['dessert']:
                # print(f)
                # print(FoodForPatient.objects.filter(portion=f, patient=patient))
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='dessert', patient=patient)
                f_qs = input_ga(portion=f, tag='dessert', patient=patient)
                selected_food.append(f_qs)

            print(selected_food)
            
            ga_calculator = Calculator(selected_food, patient.morning_carbohydrate, patient.morning_protein, patient.morning_lipid, patient.morning_energy) # 30% of total energy
            pop, hall_of_fame = ga_calculator.ga()
            print('done morning')
       
            response_data = {
                "choice 1": "",
                "choice 2": "",
                "choice 3": "",
            }            

            print(len(hall_of_fame))

            l = len(hall_of_fame)

            l = 0

            for i in range(3):
                try:
                    hall_of_fame[i]
                    l = l + 1
                except IndexError:
                    pass
            # print(l)
            
            if (l == 1):
                response_data["choice 1"] = hall_of_fame[0]
                return Response(response_data, status=status.HTTP_201_CREATED)
            elif (l == 2):                            
                response_data["choice 1"] = hall_of_fame[0]                
                response_data["choice 2"] = hall_of_fame[1]
            else:                
                response_data = {
                    "choice 1": hall_of_fame[0],
                    "choice 2": hall_of_fame[2], 
                    "choice 3": hall_of_fame[1],
                }
            
            # print(response_data)
            # serializer.save()
            
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def calculate_needed_amount_afternoon(request):
    if request.method == 'POST':
        print("request.data")
        print(request.data)
        serializer = SelectedFoodSerializer(data=request.data)

        selected_food = []
        print(serializer.is_valid())
        # Not check validivity of format, be careful!
        if serializer.is_valid() == True or (serializer.is_valid() != True):            
            patient = Patient.objects.get(pk=serializer.data['patient_id'])
            print(patient)
            selected_food = []
            for f in serializer.data['lunch']['main']:
                f_qs = input_ga(portion=f, tag='trưa', patient=patient)
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='trưa', patient=patient)
                selected_food.append(f_qs)
            for f in serializer.data['lunch']['dessert']:
                f_qs = input_ga(portion=f, tag='dessert', patient=patient)
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='dessert', patient=patient)
                selected_food.append(f_qs)

            print(selected_food)
            
            ga_calculator = Calculator(selected_food, patient.afternoon_carbohydrate, patient.afternoon_protein, patient.afternoon_lipid, patient.afternoon_energy) # 30% of total energy
            pop, hall_of_fame = ga_calculator.ga()
            print('done afternoon')
       
        
            response_data = {
                "choice 1": "",
                "choice 2": "",
                "choice 3": "",
            }            

            l = len(hall_of_fame)

            l = 0

            for i in range(3):
                try:
                    hall_of_fame[i]
                    l = l + 1
                except IndexError:
                    pass
            # print(l)
            
            if (l == 1):
                response_data["choice 1"] = hall_of_fame[0]
                return Response(response_data, status=status.HTTP_201_CREATED)
            elif (l == 2):                            
                response_data["choice 1"] = hall_of_fame[0]                
                response_data["choice 2"] = hall_of_fame[1]
            else:                
                response_data = {
                    "choice 1": hall_of_fame[0],
                    "choice 2": hall_of_fame[1],
                    "choice 3": hall_of_fame[2],
                } 
            
            # print(response_data)
            # serializer.save()
            
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def calculate_needed_amount_evening(request):
    if request.method == 'POST':
        print("request.data")
        print(request.data)
        serializer = SelectedFoodSerializer(data=request.data)

        selected_food = []
        print(serializer.is_valid())
        # Not check validivity of format, be careful!
        if serializer.is_valid() == True or (serializer.is_valid() != True):            
            patient = Patient.objects.get(pk=serializer.data['patient_id'])
            print(patient)
            selected_food = []
            for f in serializer.data['dinner']['main']:
                f_qs = input_ga(portion=f, tag='tối', patient=patient)
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='tối', patient=patient)
                selected_food.append(f_qs)
            for f in serializer.data['dinner']['dessert']:
                # f_qs = FoodForPatient.objects.get(portion=f, tag__name='dessert', patient=patient)
                f_qs = input_ga(portion=f, tag='dessert', patient=patient)
                selected_food.append(f_qs)                
            print(selected_food)
            
            ga_calculator = Calculator(selected_food, patient.afternoon_carbohydrate, patient.afternoon_protein, patient.afternoon_lipid, patient.afternoon_energy) # 30% of total energy
            pop, hall_of_fame = ga_calculator.ga()
            print('done afternoon')
       
            response_data = {
                "choice 1": "",
                "choice 2": "",
                "choice 3": "",
            }            

            l = len(hall_of_fame)

            l = 0

            for i in range(3):
                try:
                    hall_of_fame[i]
                    l = l + 1
                except IndexError:
                    pass
            # print(l)
            
            if (l == 1):
                response_data["choice 1"] = hall_of_fame[0]
                return Response(response_data, status=status.HTTP_201_CREATED)
            elif (l == 2):                            
                response_data["choice 1"] = hall_of_fame[0]                
                response_data["choice 2"] = hall_of_fame[1]
            else:                
                response_data = {
                    "choice 1": hall_of_fame[0],
                    "choice 2": hall_of_fame[1],
                    "choice 3": hall_of_fame[2],
                }
            
            # print(response_data)
            # serializer.save()
            
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)