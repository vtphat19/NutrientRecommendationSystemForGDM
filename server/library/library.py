import glob
import pandas

import logging
import pdb

courses = ('morning', 'afternoon', 'evening')
thuc_don_f = '/home/phatvo/THESIS/excels/input/Thucdon_Danhmuc.xlsx'

''' helper function to check if a cell is not a number '''
checkIsNAN = lambda x: pandas.isnull(x)   

class input_ga():
    """
    Construct a dish which has energy, macronutrients, lower bound, upper bound, and tag
    # energy should be in calories
    # macronutrients are protein, lipid, and carbohydrates with the unit of grams
    # lower bound, upper bound can be in range of [0,3].
    # tag could be one of the following: morning, afternoon, evening.
    """    
    def  __repr__(self):
        '''
            Use name of food as its representation of a food
        '''
        return str(self.name)
    
    def __init__(self, name='', energy=0, protein=0, lipid=0, carb=0, tag=0, lower_bound=0, upper_bound=0):
        '''
            Class Constructor
        '''
        self.name = name
        self.protein = protein
        self.lipid    = lipid
        self.carb   = carb
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.energy  = energy
        self.tag = tag
    def display(self):
        '''
            Display attributes of a food
        '''
        print("%s has %s calo, %s protein, %s lipid, %s carb, %s lower_bound, %s upper_bound, %s tag" % (self.name, self.energy, self.protein, self.lipid, self.carb, self.lower_bound, self.upper_bound, self.tag))

def getPatientEnergy(f, sheet_name):
    '''
        Get the patient's energy requirement from the excel sheet.
        Each sheet we has patient's energy, which is typically at the first cell
    '''    
    # Get Energy Requirement of Patient
    df = pandas.read_excel(open(f,'rb'), sheet_name=[sheet_name])    
    energy = df[sheet_name].iloc[0][0].split(' ')
    for e in energy:
        try:            
            return int(e)                  
        except ValueError:
            # Ignore exception if e is not a number!
            # logging.warning('error while calling getPatientEnergy')
            # logging.warning('ValueError occur')
            # pdb.set_trace()
            pass

def getLastRowIndx(df, name):
    try:
        lastRowIndx = df[name][df[name]['[ Food Name ]'].str.contains('Tổng NL') == True] ['[ Food Name ]'].index.values[0]
    except IndexError:
        logging.warning('error while calling getLastRowIndx')
        logging.warning('IndexError occur')

        print('there is no "Tổng NL" cell')
        # The "Tổng NL" value is generally larger than 1500
        df[name]['02.Energy'] = pandas.to_numeric(df[name]['02.Energy'], errors='coerce')                
        lastRowIndx = df[name][df[name]['02.Energy'] > 1500].index.values[0]
        
    return lastRowIndx

def lookupMonAn(df2, thucdon, course):
    start_idx = df2.index[0]
    monan = thucdon.iloc[start_idx][2]

    # if monan == "Giá, hẹ xào":
    #     print("Giá, hẹ xào")
    #     print(monan)
    #     print("end Giá, hẹ xào")
    #     start_idx = 320
    #     monan = thucdon.iloc[start_idx][2]
        


    thanhphan_list = []
    last_idx = start_idx
    last_thanhphan = None
    c = 0
    while True:
        thanhphan = thucdon.iloc[last_idx][4]        
        if last_thanhphan == thanhphan:
            c += 1
        else:
            c = 0

        energy, protein, lipid, carb = thucdon.iloc[last_idx][6], thucdon.iloc[last_idx][7], thucdon.iloc[last_idx][8], thucdon.iloc[last_idx][9]
        # print('monan: ', monan, ' ', thanhphan)
        l, u = getConstraints(monan, thanhphan, c)

        thanhphan_list.append(input_ga(
                                 name=thanhphan, 
                                 energy=energy,
                                 protein=protein,
                                 lipid=lipid,
                                 carb=carb,
                                 lower_bound=l,
                                 upper_bound=u,
                                 tag=course) )
        last_idx += 1        
        last_thanhphan = thanhphan
        if last_idx >= thucdon.shape[0]:
            break
        
        if not checkIsNAN(thucdon.iloc[last_idx][2]):
            break
        
    # print('thanhphan_list: ', thanhphan_list)
    return thanhphan_list

def getAllFiles(directory):
    '''
        Get all xls files in a directory
    '''
    path = directory
    files = []
    files = [f for f in glob.glob(path + "**/*.xls", recursive=True)]
    return files

def getSelectedFood(f, thuc_don_f, boundaries, name):
    df = pandas.read_excel(open(f,'rb'), sheet_name=[name])    
    thucdon = pandas.read_excel(open(thuc_don_f, 'rb'))
    
    c = 0
    mainFood = []
    df[name].columns = df[name].loc[1]
    res = []
    firstColName = df[name].columns[0]
    
    p = df[name][df[name][firstColName].str.contains('\w') == True] 
    
    lastRowIndx = getLastRowIndx(df, name)
    i = 0
    
    for row_index, row in p.iterrows():
        tenmon = row.loc[firstColName]
        food_name = df[name]['[ Food Name ]'].loc[row_index]

        if tenmon == 'Cơm chén lưng':
            tenmon = 'Cơm chén vừa'
        elif tenmon == 'Canh cải thảo':
            tenmon = 'Canh cải'
        # elif tenmon == 'Giá, hẹ xào':
        #     tenmon = 'Giá hẹ xào'
        elif tenmon == 'Canh bí xan':
            tenmon = 'Canh bí xanh'

   
        if 'Năng lượng' in tenmon: 
            continue

        
        if row_index > boundaries[i]:
            if i == 2:
                break

            i += 1 
        
        if tenmon.find('21h') != -1 or tenmon.find('14h') != -1:
            # Item with substrings like in the condition are not mainfood but rather dessert!
            continue

        df2 = thucdon.loc[thucdon ['Món']==tenmon]

  

        df2 = df2[['Món', '02.Energy', '04.Protein', '05.Lipid', '06.Carbohydrate']]
        
        if len(df2.index) == 0:
            print('%s, in %s. Food is not found in Thucdon_danh.xlsx: %s' %(f, name, tenmon))                    
            c += 1
            continue

        # print('tenmon', tenmon)
        elements = lookupMonAn(df2, thucdon, courses[i%3])

        for e in elements:
            mainFood.append(e)
            # mainFood.append(input_ga(name=e.name, 
            #                          energy=e.energy,
            #                          protein=e.protein,
            #                          lipid=e.lipid,
            #                          carb=e.carb,
            #                          lower_bound = 0,
            #                          upper_bound = 150,
            #                          tag=courses[i%3]))

    return mainFood, c

def getDesserts(f, name):    
    '''
        Return a list of dessert in morning, afternoon, evening
    '''
    df = pandas.read_excel(open(f,'rb'), sheet_name=None)
    
    thucdon = pandas.read_excel(open(thuc_don_f, 'rb'))
    
    i=0             
    firstColName = df[name].columns[0]
    df[name].columns = df[name].loc[1]            
    selected_food = []    
    boundaries  = [] 
    
    places = df[name][df[name]['[ Food Name ]'].str.contains("SUM")==True]
    lastRowIndx =  getLastRowIndx(df, name)
    
    numberCourses = 0
    for row_index, row in places.iterrows():
        numberCourses = numberCourses + 1
        if numberCourses >=4 :
            break
        if row_index > lastRowIndx:
            continue
        if not checkIsNAN(df[name].iloc[row_index+2]['[ Food Name ]']):
            tenmon = df[name].iloc[row_index+2]['[ Food Name ]'].strip()
            df2 = thucdon.loc[thucdon ['Món']==tenmon]
            df2 = df2[['Món', '02.Energy', '04.Protein', '05.Lipid', '06.Carbohydrate']]
            if len(df2.index) != 0:
                elements = lookupMonAn(df2, thucdon, courses[i%3])
            # print('tenmon: ', tenmon)
            # print('elements: ', elements)            
        else:
            elements = []
            dessert = None

        try:
            for e in elements:
                dessert = e
                # dessert = input_ga(name=e.name, 
                #                         energy=e.energy,
                #                         protein=e.protein,
                #                         lipid=e.lipid,
                #                         carb=e.carb,
                #                         lower_bound = 30,
                #                         upper_bound = 150,
                #                         tag=courses[i%3])
  

            boundaries.append(row_index+2)
            
        except IndexError as i:
            logging.warning('error while calling getDesserts')
            logging.warning('IndexError occur')
            IndexErrorList.append(f)
        if dessert is not None:
            selected_food.append(dessert)
        i = (i+1)%3
    return selected_food, boundaries


def getConstraints(monan, thanhphan, c):
    # print('constaints')
    # print((monan, thanhphan, c))
    '''
        c is a counter to count food that shows up multiple times in sequential order
    '''
    soluong_f = '/home/phatvo/THESIS/excels/input/SoLuongMonAn.xlsx'
    df = pandas.read_excel(open(soluong_f,'rb'))
    monan = monan.strip()
    # print('in getContraints: ', monan, thanhphan)
    temp = df.loc[df ['Món']==monan ] # monan is unique
    
    
    startPos = temp.index[0]
    lastPos = temp.index[0] + 1
    # pdb.set_trace()
    # while checkIsNAN(df.iloc[lastPos][0]):
    #     lastPos += 1
    while True:
        try:
            checkIsNAN(df.iloc[lastPos][0])
            lastPos += 1 
        except IndexError:
            break
    
    df = df.loc[startPos:lastPos]

        
    query = df.loc[df ['[ Food Name ]']==thanhphan]

    return query.iloc[c][3], query.iloc[c][4] # min, max amount
    



def getTargetNutrients(f, name):
    df = pandas.read_excel(open(f,'rb'), sheet_name=None)
    
    firstColName = df[name].columns[0]
    df[name].columns = df[name].loc[1] 
    
    lastRowIndx =  getLastRowIndx(df, name)
    
    
    places = df[name][df[name]['[ Food Name ]'].str.contains("SUM")==True]
    places = places[['[ Food Name ]', '02.Energy', '04.Protein', '05.Lipid', '06.Carbohydrate']]

    df[name] = df[name][['[ Food Name ]', '02.Energy', '04.Protein', '05.Lipid', '06.Carbohydrate']]

    meals = []
    
    for idx, p in places.iterrows():
        if idx > lastRowIndx:
            break
        e1, p1, l1, c1  = places.loc[idx][1], places.loc[idx][2], places.loc[idx][3], places.loc[idx][4]        
        e2, p2, l2, c2  = df[name].loc[idx+2][1], df[name].loc[idx+2][2], df[name].loc[idx+2][3], df[name].loc[idx+2][4]
        if not checkIsNAN(e2) and not checkIsNAN (p2) and not checkIsNAN (l2) and not checkIsNAN (c2):
            meals.append({"energy": e1 + e2, "protein": p1 + p2, "lipid": l1 + l2, "carb": c1 + c2})
        else:
            e2, p2, l2, c2 = 0, 0, 0, 0
            meals.append({"energy": e1 + e2, "protein": p1 + p2, "lipid": l1 + l2, "carb": c1 + c2})
    
    return meals
    

# def main():
#     """    thuc_don_f = '/home/phatvo/THESIS/excels/input/Thucdon_Danhmuc.xlsx'

#         DIRECTORY = '/home/phatvo/THESIS/tuan 1 16062018 - 22062018/'

#         f = DIRECTORY+ 'Nguyen T Thang 1830td.xls' #'Bùi T Thu Hà 1420td.xls'


#         desserts, boundaries  = getDesserts(f, name='T5')

#         mainFood, c = getSelectedFood(f, thuc_don_f, boundaries, 'T5') 

#         print(mainFood)
#         print(desserts)
#     """
#     l, u = getConstraints(' Miến dong (Vermicelli from Bermuda tuber)')
#     print(l, u)

# main()