from django.db import models
from django.contrib.auth.models import AbstractUser
from factors.models import Activity, StressFactor


from food.models import Portion

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.id, filename)

# In this app, we aim to have Login via email, not username. This is a common pattern in mordern mobile web apps
# Reference: https://wsvincent.com/django-login-with-email-not-username/

### Todo:
## Reset password button for user via email : send_email button

class CustomUser(AbstractUser):
    # add additional fields in here
    is_doctor = models.BooleanField(default=False)
    is_patient = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to=user_directory_path, blank=True, null=True)
    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'All User'
        verbose_name_plural = 'All Users'


# Create your models here.
class Patient(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)

    birth_date = models.DateField(blank=True, verbose_name="Birthdate (yyyy-mm-dd)", null=True)
    height = models.FloatField(verbose_name='Height (cm)', blank=True, null=True)
    weight = models.FloatField(verbose_name='Weight (kg)', blank=True, null=True)
    activity = models.ForeignKey(Activity, to_field='score', on_delete=models.DO_NOTHING,  blank=True, null=True)
    stress_factor = models.ForeignKey(StressFactor, to_field='score', on_delete=models.DO_NOTHING,  blank=True, null=True)

    energy = models.FloatField(verbose_name="Energy", blank=True, null=True)
    carbohydrate = models.FloatField(verbose_name="Carbohydrate", blank=True, null=True)
    protein = models.FloatField(verbose_name="Protein", blank=True, null=True)
    lipid = models.FloatField(verbose_name="Lipid", blank=True, null=True)
    
    morning_energy = models.FloatField(verbose_name="Morning Energy", blank=True, null=True)
    morning_carbohydrate = models.FloatField(verbose_name="Morning Carbohydrate", blank=True, null=True)
    morning_protein = models.FloatField(verbose_name="Morning Protein", blank=True, null=True)
    morning_lipid = models.FloatField(verbose_name="Morning Lipid", blank=True, null=True)    

    afternoon_energy = models.FloatField(verbose_name="Afternoon Energy", blank=True, null=True)
    afternoon_carbohydrate = models.FloatField(verbose_name="Afternoon Carbohydrate", blank=True, null=True)
    afternoon_protein = models.FloatField(verbose_name="Afternoon Protein", blank=True, null=True)
    afternoon_lipid = models.FloatField(verbose_name="Afternoon Lipid", blank=True, null=True)    

    evening_energy = models.FloatField(verbose_name="Evening Energy", blank=True, null=True)
    evening_carbohydrate = models.FloatField(verbose_name="Evening Carbohydrate", blank=True, null=True)
    evening_protein = models.FloatField(verbose_name="Evening Protein", blank=True, null=True)
    evening_lipid = models.FloatField(verbose_name="Evening Lipid", blank=True, null=True)        

    food_portion = models.ManyToManyField(Portion, through="FoodForPatient", blank=True)

    def __str__(self):
        return self.user.get_full_name() 
    
    class Meta:
        verbose_name = 'Patient'
        verbose_name_plural = 'Patients'
    
from food.models import Food
from taggit.models import Tag

class FoodForPatient(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.PROTECT)
    portion = models.ForeignKey(Portion, on_delete=models.PROTECT)
    tag     = models.ForeignKey(Tag, on_delete=None, default=1)
    created_date = models.DateField(auto_now_add=True, editable=False)
    lower_bound = models.IntegerField(verbose_name="Lower bound", default=1)
    upper_bound = models.IntegerField(verbose_name="Upper bound", default=3)
    
    def getTags(self):        
        data = ''
        # for tag in self.portion.food_id.tags.all():
        #     data = data + str(tag) + ', '
        qs = self.portion.food_id.tags.all()
        for i in range(len(qs)):
            if i != len(qs) - 1:
                data = data + str(qs[i]) + ', '
            else:
                data = data + str(qs[i])
        return data

    def __str__(self):
        return self.portion.__str__()

    getTags.short_description = 'Tags of the Food'
    # def getIngredientName(self):
    #     self.food.__str__()        

    created_date.short_description = 'Created Date'

    

    class Meta:
        unique_together = ('patient', 'portion', 'tag', )    
        verbose_name = 'Food For Patient'
        verbose_name_plural = 'Food For Patient'        

class Doctor(models.Model):   
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)

    patients = models.ManyToManyField(Patient, through='Counseling')    
    # avatar = models.ImageField(upload_to=user_directory_path)
    
    def __str__(self):
        return self.user.get_full_name()
    class Meta:
        verbose_name = 'Doctor'
        verbose_name_plural = 'Doctors'


class Counseling(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.PROTECT)
    doctor  = models.ForeignKey(Doctor, on_delete=models.PROTECT)
    date_joined = models.DateTimeField(auto_now_add=True, null=True)
    
    def getPatientName(self):
        return self.patient.__str__()

    def getDoctorName(self):
        return self.doctor.__str__()

    getPatientName.short_description = 'Patient'
    getDoctorName.short_description  = 'Treated by Doctor'
    date_joined.short_description    = " Date Joined"





        


