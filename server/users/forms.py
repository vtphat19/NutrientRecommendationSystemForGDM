from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser, Patient


class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model  = CustomUser
        fields = ('username', 'email')

class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('username', 'email')

class PatientForm(forms.ModelForm):    
    class Meta:        
        model = Patient
        fields = ['energy']