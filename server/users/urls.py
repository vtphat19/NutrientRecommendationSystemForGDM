# from django.urls import path
# from rest_framework.urlpatterns import format_suffix_patterns
# from . import views

# urlpatterns = [
#     # path('snippets/', views.PatientView.as_view()),
#     path('patient/<int:pk>/', views.PatientView),
# ]

# urlpatterns = format_suffix_patterns(urlpatterns)

from .views import PatientView, DoctorView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'patient', PatientView, basename='patient')
router.register(r'doctor', DoctorView, basename='doctor')
urlpatterns = router.urls