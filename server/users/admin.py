from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django import forms
from datetime import date

from django.http import HttpResponseRedirect
from .forms import CustomUserCreationForm, CustomUserChangeForm, PatientForm
from .models import CustomUser, Doctor, Patient, Counseling, FoodForPatient
from food.models import Portion
from django.db import IntegrityError


from django.contrib.admin.views.main import ChangeList
from django.core.paginator import EmptyPage, InvalidPage, Paginator

# # Solution to calcuate age, 
# # source: Danny W. Adair,  https://stackoverflow.com/questions/2217488/age-from-birthdate-in-python
def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form     = CustomUserChangeForm
    model    = CustomUser    
    list_display = ['email', 'username', 'first_name', 'last_name', 'is_doctor', 'is_patient']

class CounselingInline(admin.TabularInline):
    model = Counseling
    extra = 1

from django.utils.safestring import mark_safe


class FoodForPatientInline(admin.TabularInline):
    show_change_link = True
    model = FoodForPatient    
    readonly_fields = ('getTags',)
    fields = ['portion', 'lower_bound', 'upper_bound', 'tag', 'getTags']
    extra = 1 
    

class CounselingAdmin(admin.ModelAdmin):
    list_display = ('getPatientName', 'getDoctorName', 'date_joined')


class DoctorAdmin(admin.ModelAdmin):
    inlines = (CounselingInline, )

class PatientAdmin(admin.ModelAdmin):
    """
    # Estimated Energy Requirement (kcal/day) = Total Energy Expenditure
    # EER = 354 − (6.91 × age [y]) + PA × [(9.36 × weight [kg]) + (726 v height [m])]
    # compared correctly with https://fnic.nal.usda.gov/fnic/dri-calculator/results.php 
    """
    inlines = [CounselingInline,  FoodForPatientInline, ]        
    change_form_template = "users/patient_changeform.html"        
    

    def response_change(self, request, obj):
        ### Total energy
        if "_calculate_energy" in request.POST:
            patient = Patient.objects.get(pk=obj.pk) # another way: self.get_queryset(request).filter(name=obj.pk
            age = calculate_age(patient.birth_date)
            height = patient.height / 100
            weight = patient.weight
            activity_level = patient.activity.score # PA 
            energy = 354 - (6.91 * age) + activity_level * ( (9.36 * weight) + (726 * height) ) 
            patient.energy = energy
            patient.save()            
            return HttpResponseRedirect(".")
        if "_calculate" in request.POST:
            patient = Patient.objects.get(pk=obj.pk)
            patient.carbohydrate = float(request.POST['_carb_ratio'])  * patient.energy /4
            patient.protein = float(request.POST['_protein_ratio'])  * patient.energy /4
            patient.lipid = float(request.POST['_lipid_ratio'])  * patient.energy /9
            patient.save()            
            return HttpResponseRedirect(".")

        ### Morning
        if "_calculate_energy_morning" in request.POST:
            patient = Patient.objects.get(pk=obj.pk) 
            patient.morning_energy = float(request.POST['_energy_ratio_morning']) * patient.energy
            
            patient.save()            
            return HttpResponseRedirect(".")
        if "_calculate_nutrients_morning" in request.POST:
            patient = Patient.objects.get(pk=obj.pk)
            patient.morning_carbohydrate = float(request.POST['_carb_ratio_morning'])  * patient.morning_energy /4
            patient.morning_protein = float(request.POST['_protein_ratio_morning'])  * patient.morning_energy /4
            patient.morning_lipid = float(request.POST['_lipid_ratio_morning'])  * patient.morning_energy /9
            patient.save()            
            return HttpResponseRedirect(".")        
        
        ### Afternoon
        if "_calculate_energy_afternoon" in request.POST:
            patient = Patient.objects.get(pk=obj.pk) 
            patient.afternoon_energy = float(request.POST['_energy_ratio_afternoon']) * patient.energy            
            patient.save()            
            return HttpResponseRedirect(".")
        if "_calculate_nutrients_afternoon" in request.POST:
            patient = Patient.objects.get(pk=obj.pk)
            patient.afternoon_carbohydrate = float(request.POST['_carb_ratio_afternoon'])  * patient.afternoon_energy /4
            patient.afternoon_protein = float(request.POST['_protein_ratio_afternoon'])  * patient.afternoon_energy /4
            patient.afternoon_lipid = float(request.POST['_lipid_ratio_afternoon'])  * patient.afternoon_energy /9
            patient.save()            
            return HttpResponseRedirect(".")     

        ### Evening                   
        if "_calculate_energy_evening" in request.POST:
            patient = Patient.objects.get(pk=obj.pk) 
            patient.evening_energy = float(request.POST['_energy_ratio_evening']) * patient.energy            
            patient.save()            
            return HttpResponseRedirect(".")
        if "_calculate_nutrients_evening" in request.POST:
            patient = Patient.objects.get(pk=obj.pk)
            patient.evening_carbohydrate = float(request.POST['_carb_ratio_evening'])  * patient.evening_energy /4
            patient.evening_protein = float(request.POST['_protein_ratio_evening'])  * patient.evening_energy /4
            patient.evening_lipid = float(request.POST['_lipid_ratio_evening'])  * patient.evening_energy /9
            patient.save()            
            return HttpResponseRedirect(".")     

        if "_load_all_food" in request.POST:
            patient = Patient.objects.get(pk=obj.pk)
            all_food_portion = Portion.objects.all()
            for f in all_food_portion:
                qs = f.food_id.tags.all()
                if len(qs) > 0:
                    for t in qs:
                        try:                                             
                            FoodForPatient(patient=patient, portion=f, tag=t).save()
                        except IntegrityError as e:
                            pass
                else: # use default tag
                    try:                                             
                        FoodForPatient(patient=patient, portion=f).save()
                    except IntegrityError as e:
                        pass
            return HttpResponseRedirect(".")

        return super().response_change(request, obj)        

class FoodForPatientAdmin(admin.ModelAdmin):
    list_display = ('id','patient', 'portion', 'tag', 'lower_bound', 'upper_bound')
    search_fields  = ('patient', 'tag', )   
    list_filter = ('patient', 'tag', )   
    ordering = ('id', 'patient',)

admin.site.register(Doctor, DoctorAdmin)
admin.site.register(Patient, PatientAdmin)
admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Counseling, CounselingAdmin)
admin.site.register(FoodForPatient, FoodForPatientAdmin)

from allauth import socialaccount